## What is Wysifield

The Wysifield module provides filters/plugins for the WYSIWYG that allow for the embedding of entities inside of other entities. Making an entity embeddable is as easy as adding the wysifield to your entity and enabling the button in the WYSIWYG configuration.

Currently Wysifield only supports using the WYSIWYG API module with the CKeditor plugin.

## Supported Embeddable Entities

* Nodes
* Fieldable Panel Panes

## Installation

* Install as you would any Drupal module. See https://www.drupal.org/documentation/install/modules-themes/modules-7
* Please note that the installation process with create a base field named 'wysifield_field'.

## Configuration

* Add the wysifield_field field that was created by the wysifield installer to the entity bundle that you would like to make available for embedding.
* Go to admin/config/content/formats and enable the Wysifield Formatter for the text formats that you want to use Wysifield in.
* Go to the WYSIWYG configuration and enable the corresponding button for your embeddable entity.
* Note, you will need a button and preview image for each embeddable entity.

## Acknowledgments

Wysifield began as an internal project at Mediacurrent for the Weather.com project. That project was the brain child of Jason Smith, who created it along with the help of James Rutherford. The value of this project beyond it's initial purpose became clear and the Wysifield project was born.

Maintainers: Bob Kepford, James Rutherford, and Jason Want.
Thanks to many of my colleagues at Mediacurrent including, Andrew Riley, James Rutherford, Jason Smith, Jason Want, Jeff Diecks, and others.
Mediacurrent, for funding many of the development hours.
